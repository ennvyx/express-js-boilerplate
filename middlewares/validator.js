const { body, param, query, validationResult } = require("express-validator");

exports.getUserValidation = () => {
  return [
    query("name").exists().withMessage("name is required"),
    query("age").exists().withMessage("age is required"),
    param("id").exists().withMessage("id is required"),
  ];
};
exports.updateUserValidation = () => {
  return [
    param("id").exists().withMessage("id is required"),
    body("user_id").exists().withMessage("userId is required"),
    body("transaction_details")
      .exists()
      .withMessage("transactionDetails is required"),
  ];
};

exports.newTransactionValidation = () => {
  return [
    body("user_id").exists().withMessage("userId is required"),
    body("transaction_details")
      .exists()
      .withMessage("transactionDetails is required"),
  ];
};

exports.getTransactionValidation = () => {
  return [param("id").exists().withMessage("id is required")];
};

exports.validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(400).json({
    errors: extractedErrors,
  });
};
