var DataTypes = require("sequelize").DataTypes;
var _ikimodal_bulk = require("./ikimodal_bulk");
var _transaction = require("./transaction");
var _upload = require("./upload");
var _user = require("./user");

function initModels(sequelize) {
  var ikimodal_bulk = _ikimodal_bulk(sequelize, DataTypes);
  var transaction = _transaction(sequelize, DataTypes);
  var upload = _upload(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);

  transaction.belongsTo(user, { as: "user", foreignKey: "user_id"});
  user.hasMany(transaction, { as: "transactions", foreignKey: "user_id"});

  return {
    ikimodal_bulk,
    transaction,
    upload,
    user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
