const express = require("express");
const response = require("../../components/response");
const router = express.Router();

const users = require("./users");
// const perhitungan = require("./perhitungan");
// const transaction = require("./transaction");

const index = function (req, res, next) {
  response.res404(res);
};

router.use("/users", users);
// router.use("/perhitungan", perhitungan);
// router.use("/transaction", transaction);
router.all("/", index);

router.all("*", index);

module.exports = router;
