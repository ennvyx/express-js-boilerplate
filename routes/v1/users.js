const apicache = require("apicache");
const cache = apicache.middleware;
const response = require("../../components/response");
const validator = require("../../middlewares/validator");
const express = require("express");
const router = express.Router();

const usersController = require("../../controllers/users");

const index = function (req, res, next) {
  response.res404(res);
};

router.route("/list").get((req, res) => {
  usersController.getData(req, res);
});

router.route("/").post((req, res) => {
  usersController.createUser(req, res);
});
router.route("/test").post((req, res) => {
  usersController.test(req, res);
});

/* validator.sendPaylaterNotification(),
    validator.validate,
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        response.res400(res, errors.array());
      } else {
        controller.sendNotification(req, res);
      }
    } */

router
  .route("/")
  .get(validator.getUserValidation(), validator.validate, (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      response.res400(res, errors.array());
    } else {
      usersController.getUser(req, res);
    }
  });

router.route("/:id").put((req, res) => {
  usersController.updateUser(req, res);
});

router.route("/:id").delete((req, res) => {
  usersController.deleteUser(req, res);
});

router.route("/history/:transId").get((req, res) => {
  usersController.findTransactionDetail(req, res);
});

router.route("/history/").get((req,res) => {
  usersController.findTransactionHistory(req,res);
});

router.all("*", index);

module.exports = router;
