"use strict";

const { user } = require("../components/database");
const {maindb } = require("../components/database");
const response = require("../components/response");
const Crypto = require("crypto");
const jwt = require("jsonwebtoken");
const { v4 } = require("uuid");
const axios = require("axios");
// const { post } = require("../routes/v1/transaction");
const { QueryTypes } = require("sequelize");
const url = process.env.FUSIONAUTH_URL;

exports.getData = (req, res) => {
  user
    .findAll()
    .then((hasil) => {
      response.res200(res, hasil);
    })
    .catch((err) => {
      response.res500(res, err);
    });
};

exports.test = (req, res) => {
  console.log(req.headers);
};


